;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; coding: utf-8-unix -*- ;;;;;;;;80

(defpackage #:cartesian-system
  (:use :cl :asdf))

(in-package :cartesian-system)

(defsystem cartesian
    :name "cartesian"
    :version "1.0.0"
    :author "Paul Sexton"
    :description "Utilities for working with 2-dimensional coordinates."
    :long-description "Utilities for working with 2-dimensional coordinates."
    :serial t
    :components ((:file "cartesian"))
    :depends-on ("iterate"
                 "alexandria"
                 "fare-memoization"
                 "closer-mop"
                 "defstar"))
