;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; coding: utf-8-unix -*- ;;;;;;;;80

(in-package :cl-user)

(declaim (optimize (speed 0) (safety 3) (debug 3)))

(defpackage :cartesian
  (:nicknames :cart)
  (:use
   :cl
   :alexandria  ; for once-only, iota, shuffle, clamp, if-let, last-elt
   :closer-mop
   :iterate :fare-memoization :defstar)
  (:shadow #:with-gensyms #:norm)
  (:shadowing-import-from :alexandria #:set-equal)
  (:shadowing-import-from :closer-mop
                          #:standard-generic-function
                          #:slot-definition-initform
                          #:defgeneric
                          #:defclass
                          #:defmethod
                          #:standard-method
                          #:ensure-generic-function
                          #:standard-class)
  (:export
   #:*invert-y-axis?*
   #:=coord=
   #:=coord-pair=
   #:=direction=
   ;; Polar coordinates
   #:polar-rho
   #:polar-theta
   #:polar->cartesian-x
   #:polar->cartesian-y
   ;; Radians and degrees
   #:radians->degrees
   #:degrees->radians
   #:degrees-in-range?
   #:degrees+
   ;; Cartesian coordinates: points
   #:dist
   #:bear-dist
   #:dist^2
   #:coord-sort
   #:group-coords
   #:find-coords-touching
   #:print-coords
   #:coords-closer
   #:midpoint
   ;; Cartesian coordinates: lines & vectors
   #:bresenham-line
   #:bresenham-line-vector
   #:do-bresenham-line
   #:bresenham-line-generator
   #:random-path
   #:in-line-from
   #:line->vector
   #:vector-normalise
   #:vector-angle
   #:vector-sign
   #:vector-right
   #:vector-left
   #:vector+
   #:vector-
   #:vector-reverse
   #:vector-flip-y-axis
   #:vector*
   #:vector/
   #:vector-ceiling
   #:dot-product
   #:vector-length
   #:cosine-between-vectors
   #:angle-between-vectors
   #:line-gradient
   #:line-y-intercept
   #:line-intersection
   #:line-mid-point
   #:points->lines
   #:area->coords
   #:is-point-on-line?
   #:vector-gradient
   #:area-coords->index
   #:area-index->x
   #:area-index->y
   ;; Cartesian coordinates: rectangles
   #:coordinates-in-area?
   #:coordinates-on-area-border?
   #:areas-overlap?
   #:areas-overlap-by-1?
   #:areas-touch?
   #:area-contains-area?
   #:circle-overlaps-rectangle?
   #:do-area
   #:do-area-randomly
   #:in-area-from
   #:do-borders
   #:do-borders-no-corners
   #:do-array-borders
   #:on-array-border?
   #:random-coordinates
   #:random-edge-coordinates
   #:coord-groups-in-line?
   #:area-of-polygon
   #:point-in-polygon?
   #:polygons-overlap?
   #:polygon-bounds
   #:random-coordinates-in-polygon
   #:print-polygons
   ;; Numberpad "directions" (1-9)
   #:dir->dx
   #:dir->dy
   #:coords->dir
   #:dx-dy->dir
   #:opposite-dir
   #:x+dir
   #:y+dir
   #:left-dir
   #:right-dir
   #:soft-left-dir
   #:soft-right-dir
   #:hard-left-dir
   #:hard-right-dir
   #:dir->degrees
   #:dir->vector
   #:dir->corner
   #:dir-diff
   #:cartesian-coordinates->dir
   #:dir->long-compass-direction
   #:dir->short-compass-direction
   #:random-direction
   ;; 2D arrays
   #:make-2d-array
   #:do-2d-array
   #:do-2d-array-randomly
   #:array-xdim
   #:array-ydim
   #:array-at
   #:print-2d-array
   #:flip-2d-array-vertical
   #:flip-2d-array-horizontal
   #:rotate-2d-array
   #:find-non-empty-row-in-2d-array
   #:find-non-empty-column-in-2d-array
   #:out-of-2d-array-bounds?
   #:in-adjacent-coords
   #:coords-adjacent?
   #:do-for-adjacent-coords
   #:random-adjacent-coords
   #:coords-at-distance
   #:coords-within-distance
   #:of-array
   ;; Rectangle class ;;;;
   #:rectangle
   #:rect-minx
   #:rect-maxx
   #:rect-miny
   #:rect-maxy
   #:rect-xdim
   #:rect-ydim
   #:rect-area
   #:rect-longest-axis
   #:rect-centre-x
   #:rect-centre-y
   #:do-rectangle
   #:do-rectangle-borders
   #:in-rectangle?
   #:on-rectangle-border?
   #:rectangles-at
   #:rect-overlaps-area?
   #:rect-touches-area?
   #:rectangles-overlap?
   #:rectangles-touch?
   #:rectangle-contains-rectangle?
   #:rectangle-can-contain-rectangle?
   #:inner-rectangle
   #:move-rectangle
   #:rotate-rectangle
   #:flip-rectangle-vertical
   #:flip-rectangle-horizontal
   #:random-point-in-rectangle
   #:with-random-point-in-rectangle
   #:print-rectangles
   ;; 2D matrix class ;;;;
   #:<2d-matrix>
   #:matrix-array
   #:matrix-xdim
   #:matrix-ydim
   #:matrix-initial-element
   #:matrix-initial-function
   ;;#:matrix-matrix
   #:<char-matrix>
   #:do-matrix
   #:matrix-value-at
   #:print-matrix
   #:matrix-blit
   #:random-matrix-value-in-rectangle
   #:map-matrix
   ))


(in-package :cartesian)


;;; [[Distance between two points]]
;;; [[Areas]] rectangles x1,y1 -- x2,y2
;;; [[2D Arrays]]
;;; [[Rectangle class]]
;;; [[2D Matrix class]]
;;; [[Lists of coordinates]]
;;; [[Iteration through coordinates]]
;;; [[Directions]]
;;; [[Polar coordinates]]
;;; [[Vectors]]
;;; [[Lines]]
;;; [[Bresenham line algorithm]]
;;; [[Polygons]]


(deftype =coord= () 'real)
(deftype =coord-pair= () '(cons =coord= =coord=))
(deftype =2d-array= (&optional (elt-type '*)) `(array ,elt-type 2))
(deftype =2d-vector= () `(cons =coord= =coord=))
(deftype =distance= () 'real)
(deftype =degrees= () `(real -360 360))
(deftype =direction= () `(integer 1 9))
(deftype =sign= () `(integer -1 1))

(defvar *invert-y-axis?* nil
  "If nil (default), Y increases on moving 'up'. This is the normal behaviour
for cartesian coordinates in mathematics. If non-nil, Y increases on moving
'down', which is the normal behaviour for computer graphics. This variable
mainly affects the 'direction' functions, and functions which print out
coordinates.")


(defun* greatest ((fn function) (seq sequence))
  "FN is a function that accepts a single argument and returns a numerical
value. Return the element in SEQ for which (FN element) returns the highest
number."
  (let ((results (iterate (for item in-sequence seq)
                   (collect (funcall fn item)))))
    (elt seq (position (apply #'max results) results))))


(defun* least ((fn function) (seq sequence))
  "FN is a function that accepts a single argument and returns a numerical
value. Return the element in SEQ for which (FN element) returns the lowest
number."
  (let ((results (iterate (for item in-sequence seq)
                   (collect (funcall fn item)))))
    (elt seq (position (apply #'min results) results))))


(defun* (sign -> =sign=) ((n real))
  (truncate (signum n)))


(defun rand-between (min max)
  (+ min (random (abs (1+ (- max min))))))


(defun* (left-elts -> sequence) ((seq sequence) (n (integer 0)))
  "Return the subsequence containing the initial N elements of SEQ."
  (subseq seq 0 (min (length seq) n)))


(defun* (range -> list) ((lo integer) (hi integer))
  (if (> lo hi)
      (reverse (range hi lo))
      (iota (1+ (- hi lo)) :start lo)))


(defun* (map-array -> array) ((fn function) (a array))
  "Return a vector or array of the same dimensions as A, where each
element in the new array is the value returned by (FN OLDVAL)."
  (let ((new (make-array (array-dimensions a))))
    (dotimes (i (array-total-size a))
      (setf (row-major-aref new i) (funcall fn (row-major-aref a i))))
    new))


(defun shallow-copy-object (original)
  (let* ((class (class-of original))
         (copy (allocate-instance class)))
    (dolist (slot (mapcar #'slot-definition-name (class-slots class)))
      (when (slot-boundp original slot)
        (setf (slot-value copy slot)
              (slot-value original slot))))
    copy))



;; LISP-UNIT -- unit testing library
;; (define-test left-elts
;;   ;; (run-tests): runs all tests in current package
;;   ;; (run-tests A B C...): run named tests
;;   ;; (run-all-tests PACKAGE): runs all tests in given package
;;   ;; (remove-tests): remove all tests in current package
;;   ;; (remove-tests A B C...)
;;   ;; (use-debugger BOOL): activate/deactivate debugger handling of any
;;   ;;    errors that occur inside tests (default = inactive)
;;   ;;
;;   ;; Assertions (inside 'define-test'):
;;   ;;
;;   ;; assert-equal, -eq, -eql EXPECTED FORM
;;   ;; assert-equality PREDICATE expected form
;;   ;; assert-true, assert-false FORM
;;   ;; assert-prints "output" FORM -- form must print "output" to stdout
;;   ;; assert-expands EXPANSION FORM -- macro must expand to EXPANSION
;;   ;; assert-error CONDITION-TYPE FORM -- form must raise error of given type
;;   (assert-equal "fo" (left-elts "foobar" 2))
;;   (assert-equal "" (left-elts "foobar" 0))
;;   (assert-equal "" (left-elts "" 5))
;;   (assert-equal "" (left-elts "" 0))
;;   (assert-equal "foobar" (left-elts "foobar" 500)))


(defmacro with-gensyms ((&rest bindings) &body body)
  "Executes a series of forms with each var bound to a fresh,
uninterned symbol. See http://www.cliki.net/WITH-UNIQUE-NAMES"
  `(let ,(mapcar #'(lambda (var)
                     (check-type var symbol)
                     `(,var (gensym ,(string var))))
          bindings)
     ,@body))


;;; <<Degrees and radians>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(define-memo-function radians->degrees (radians)
  "Return RADIANS converted to degrees (0 to 360)."
  (* radians (/ 180 pi)))

(define-memo-function degrees->radians (deg)
  "Return RADIANS converted to degrees (0 to 360)."
  (* deg (/ pi 180)))


(defun* (degrees-in-range? -> boolean) ((degrees =degrees=)
                                        (dmin =degrees=) (dmax =degrees=))
  "Is DEGREES within the range of degrees DMIN to DMAX? The range must be
specified in an anticlockwise direction, where 0 degrees is 'east' and 90
degrees is 'north', and dmax is in an anticlockwise direction from dmin.
If dmax is not anticlockwise to dmin, the function assumes you are
describing a range of degrees obtained by going 'full circle' in an
anticlockwise direction."
  (when (< degrees 0) (incf degrees 360))
  (when (< dmin 0) (incf dmin 360))
  (when (< dmax 0) (incf dmax 360))
  (unless (zerop degrees) (setf degrees (mod degrees 360)))
  (unless (zerop dmin) (setf dmin (mod dmin 360)))
  (unless (zerop dmax) (setf dmax (mod dmax 360)))
  (cond
    ((> dmin dmax)                    ; cases where range crosses 0/360 degrees
     (or (<= degrees dmax)
         (>= degrees dmin)))
    ((<= dmin dmax)                     ; "normal" cases, dmin < dmax
     (and (>= degrees dmin)
          (<= degrees dmax)))))


(defun* (degrees+ -> =degrees=) ((d1 =degrees=) (d2 =degrees=))
  "Return the sum of adding two DEGREES; deal with cases where the sum is
negative, or wraps past 360 degrees"
  (let ((sum (+ d1 d2)))
    (iterate
      (while (< sum 0))
      (incf sum 360))
    (mod sum 360)))


;;; <<Distance between two points>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;  * Approximate distance between two points. (from Angband source)
;;;  *
;;;  * When either the X or Y component dwarfs the other component,
;;;  * this function is almost perfect, and otherwise, it tends to
;;;  * over-estimate about one grid per fifteen grids of distance.
;;;  *
;;;  * Algorithm: hypot(dy,dx) = max(dy,dx) + min(dy,dx) / 2
;;;
;;; Note: This function is FASTER unmemoized.

(defun* (dist -> =distance=) ((x1 fixnum) (y1 fixnum)
                              (x2 fixnum) (y2 fixnum))
  "Returns the integer distance between X1,Y1 and X2,Y2. All coordinates must
also be integers."
  (let ((ax (if (> x1 x2) (- x1 x2) (- x2 x1)))
        (ay (if (> y1 y2) (- y1 y2) (- y2 y1))))
    (if (> ay ax)
        (+ ay (floor ax 2))
        (+ ax (floor ay 2)))))


;;; The function below is an alternative way of calculating distance.
;;; Seems to be no faster than DIST.
(defun* (bear-dist -> =distance=) ((x1 fixnum) (y1 fixnum)
                                   (x2 fixnum) (y2 fixnum))
  (let ((ax (if (> x1 x2) (- x1 x2) (- x2 x1)))
        (ay (if (> y1 y2) (- y1 y2) (- y2 y1))))
    (ash (+ ax ay (max ax ay)) -1)))


(defun* (dist^2 -> =distance=) ((x1 =coord=) (y1 =coord=)
                                (x2 =coord=) (y2 =coord=))
  "Returns the square of the distance from X1,Y1 to X2,Y2."
  (+ (* (- x2 x1) (- x2 x1)) (* (- y2 y1) (- y2 y1))))


(defun* (coords-closer -> (values =coord= =coord=)) ((x fixnum) (y fixnum)
                                                     (targetx fixnum)
                                                     (targety fixnum)
                                                     &key (diagonal-ok? t))
  "If we are at the coordinates X,Y, and we want to get to TARGETX,TARGETY,
return the coordinates of the adjacent X,Y position which is nearest to
the target position."
  (cond
    ((and (= x targetx) (= y targety))
     (values x y))
    ((= x targetx)
     (values x (+ y (sign (- targety y)))))
    ((= y targety)
     (values (+ x (sign (- targetx x))) y))
    ((not diagonal-ok?)
     (if (> (abs (- x targetx)) (abs (- y targety)))
         (values (+ x (sign (- targetx x))) y)
         (values x (+ y (sign (- targety y))))))
    (t
     (values (+ x (sign (- targetx x)))
             (+ y (sign (- targety y)))))))

     ;; (let* ((bline (bresenham-line-generator x y (- targetx x) (- targety y)))
     ;;        (coords (funcall bline)))
     ;;   (values (car coords) (cdr coords))))))


(defun* (coords-adjacent? -> boolean) ((x1 =coord=) (y1 =coord=)
                                       (x2 =coord=) (y2 =coord=))
  "Return true if (X2,Y2) is immediately adjacent to (X1,Y1), ie in one of
the eight positions touching (X1,Y1). Returns false if the two positions are
the same."
  (and (= 1 (abs (- x1 x2)))
       (= 1 (abs (- y1 y2)))))


(defun* (random-adjacent-coords -> =coord-pair=)
    ((x =coord=) (y =coord=) &optional (radius 1))
  "Return a cons pair (X . Y) specifying a position within RADIUS squares
of the specified position."
  (cons
   (+ x (* (1+ (random radius)) (if (zerop (random 2)) 1 -1)))
   (+ y (* (1+ (random radius)) (if (zerop (random 2)) 1 -1)))))



(defun* (midpoint -> (cons real real)) (&rest coord-pairs)
  "Given an arbitrary number of (X . Y) coordinate pairs, return the coordinates
of their midpoint."
  (assert coord-pairs)
  (let* ((x-coords (mapcar #'car coord-pairs))
         (y-coords (mapcar #'cdr coord-pairs)))
    (cons (/ (apply #'+ x-coords) (length x-coords))
          (/ (apply #'+ y-coords) (length y-coords)))))


;;; <<Areas>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun* (coordinates-in-area? -> boolean) ((x =coord=) (y =coord=)
                                           (x1 =coord=) (y1 =coord=)
                                           (x2 =coord=) (y2 =coord=))
  (and (<= (min x1 x2) x (max x1 x2))
       (<= (min y1 y2) y (max y1 y2))))


(defun* (coordinates-on-area-border? -> boolean) ((x =coord=) (y =coord=)
                                                  (x1 =coord=) (y1 =coord=)
                                                  (x2 =coord=) (y2 =coord=))
  (or (= x x1) (= x x2)
      (= y y1) (= y y2)))


(defun* (areas-overlap? -> boolean) ((ax1 =coord=) (ay1 =coord=)
                                     (ax2 =coord=) (ay2 =coord=)
                                     (bx1 =coord=) (by1 =coord=)
                                     (bx2 =coord=) (by2 =coord=))
  (let ((a-minx (min ax1 ax2)) (a-maxx (max ax1 ax2))
        (a-miny (min ay1 ay2)) (a-maxy (max ay1 ay2))
        (b-minx (min bx1 bx2)) (b-maxx (max bx1 bx2))
        (b-miny (min by1 by2)) (b-maxy (max by1 by2)))
    (not (or (< a-maxx b-minx)
             (< a-maxy b-miny)
             (> a-minx b-maxx)
             (> a-miny b-maxy)))))


(defun* (areas-touch? -> boolean) ((ax1 =coord=) (ay1 =coord=)
                                   (ax2 =coord=) (ay2 =coord=)
                                   (bx1 =coord=) (by1 =coord=)
                                   (bx2 =coord=) (by2 =coord=))
  "Are the two areas immediately adjacent, but not actually overlapping?"
  (let ((a-minx (min ax1 ax2)) (a-maxx (max ax1 ax2))
        (a-miny (min ay1 ay2)) (a-maxy (max ay1 ay2))
        (b-minx (min bx1 bx2)) (b-maxx (max bx1 bx2))
        (b-miny (min by1 by2)) (b-maxy (max by1 by2)))
    (and (not (areas-overlap? ax1 ay1 ax2 ay2 bx1 by1 bx2 by2))
         (areas-overlap? (1- a-minx) (1- a-miny) (1+ a-maxx) (1+ a-maxy)
                         b-minx b-miny b-maxx b-maxy))))



(defun* (areas-overlap-by-1? -> (member :left :right :top :bottom nil))
    ((ax1 =coord=) (ay1 =coord=) (ax2 =coord=) (ay2 =coord=)
     (bx1 =coord=) (by1 =coord=) (bx2 =coord=) (by2 =coord=))
  "Two areas overlap by only one tile thickness (one row/column/tile).
Returns the 'side' of the first area (A) where the overlap occurs."
  (let ((a-minx (min ax1 ax2)) (a-maxx (max ax1 ax2))
        (a-miny (min ay1 ay2)) (a-maxy (max ay1 ay2))
        (b-minx (min bx1 bx2)) (b-maxx (max bx1 bx2))
        (b-miny (min by1 by2)) (b-maxy (max by1 by2)))
    (cond
      ((and (= a-minx b-maxx)
            (or (<= b-miny a-miny b-maxy)
                (<= b-miny a-maxy b-maxy)))
       :left)
      ((and (= a-maxx b-minx)
            (or (<= b-miny a-miny b-maxy)
                (<= b-miny a-maxy b-maxy)))
       :right)
      ((and (= a-miny b-maxy)
            (or (<= b-minx a-minx b-maxx)
                (<= b-minx a-maxx b-maxx)))
       :top)
      ((and (= a-maxy b-miny)
            (or (<= b-minx a-minx b-maxx)
                (<= b-minx a-maxx b-maxx)))
       :bottom)
      (t nil))))


(defun* (area-contains-area? -> boolean)
    ((ax1 =coord=) (ay1 =coord=) (ax2 =coord=) (ay2 =coord=)
     (bx1 =coord=) (by1 =coord=) (bx2 =coord=) (by2 =coord=))
  "Is area A contained within area B?"
  (let ((maxx (max bx1 bx2)) (maxy (max by1 by2))
        (minx (min bx1 bx2)) (miny (min by1 by2)))
    (and (<= minx ax1 maxx)
         (<= minx ax2 maxx)
         (<= miny ay1 maxy)
         (<= miny ay2 maxy))))


(defun* (circle-overlaps-rectangle? -> boolean)
    ((centrex =coord=) (centrey =coord=)
     (radius =distance=)
     (x1 =coord=) (y1 =coord=) (x2 =coord=) (y2 =coord=))
  "Return true if the circle centred at CENTREX,CENTREY and with radius RADIUS,
overlaps the rectangle X1,Y1 --> X2,Y2."
  (let ((maxx (max x1 x2)) (maxy (max y1 y2))
        (minx (min x1 x2)) (miny (min y1 y2)))
    (cond
      ((coordinates-in-area? centrex centrey x1 y1 x2 y2)
       t)
      ((not (coordinates-in-area? centrex centrey (- x1 radius) (- y1 radius)
                                  (+ x2 radius) (+ y2 radius)))
       nil)
      ((and (<= minx centrex maxx)
            (<= (- miny radius) centrey (+ maxy radius)))
       t)
      ((and (<= miny centrey maxy)
            (<= (- minx radius) centrex (+ maxx radius)))
       t)
      (t
       nil))))


(defun* (area->coords -> list) ((x1 =coord=) (y1 =coord=)
                                (x2 =coord=) (y2 =coord=))
  (list (cons x1 y1)
        (cons x2 y1)
        (cons x2 y2)
        (cons x1 y2)))


(defmacro do-area ((x y x1 y1 x2 y2) &body body)
  "Given 2d array bounded by X1,Y1 --> X2,Y2.
Iterate through all points in this array, setting the
placeholders X and Y to the coordinates of each such point in turn."
  (once-only (x1 y1 x2 y2)
    (with-gensyms (do-area-body)
      `(flet ((,do-area-body (,x ,y) ,@body))
         (loop for ,x from (min ,x1 ,x2) upto (max ,x1 ,x2)
               do (loop for ,y from (min ,y1 ,y2) upto (max ,y1 ,y2)
                        do (,do-area-body ,x ,y)))))))


(defmacro-driver (FOR x.y :IN-AREA-FROM x1.y1 :TO x2.y2)
    "Usage:
: (for (x . y) :in-area-from (0 . 0) :to (10 . 10))"
  (let ((kwd (if generate 'generate 'for)))
    (with-gensyms (x y minx miny maxx maxy)
      `(progn
         (with ,minx = (min ,(car x1.y1) ,(car x2.y2)))
         (with ,miny = (min ,(cdr x1.y1) ,(cdr x2.y2)))
         (with ,maxx = (max ,(car x1.y1) ,(car x2.y2)))
         (with ,maxy = (max ,(cdr x1.y1) ,(cdr x2.y2)))
         (with ,x = (1- ,minx))
         (with ,y = ,miny)
         (,kwd ,x.y next (progn
                           (incf ,x)
                           (when (> ,x ,maxx)
                             (setf ,x ,minx)
                             (incf ,y))
                           (if (> ,y ,maxy) (terminate))
                           (cons ,x ,y)))))))



(defmacro do-area-randomly ((iterx itery x1 y1 x2 y2)
                            &body body)
  "Iterate through all tiles from X1,X2 --> Y1,Y2,
starting with a random tile. Within the body of the form, bind TILE to
the value of each tile in turn, and ITERX and ITERY to that tile's
coordinates."
  (once-only (x1 y1 x2 y2)
    (with-gensyms (area indices index)
      `(let* ((,area (* (1+ (abs (- ,x2 ,x1))) (1+ (abs (- ,y2 ,y1)))))
             (,indices (shuffle (iota (1- ,area) :start 0))))
         (loop for ,index across ,indices
               do (destructuring-bind (,iterx . ,itery)
                      (area-index->coords ,index ,x1 ,y1 ,x2 ,y2)
                    ,@body))))))


(defmacro-driver (FOR x.y :RANDOMLY-IN-AREA-FROM x1.y1
                   :TO x2.y2)
  "Usage:
: (for (x . y) :randomly-in-area-from (0 . 0) :to (10 . 10))"
  (let ((kwd (if generate 'generate 'for)))
    (with-gensyms (minx miny maxx maxy area indices i)
      `(progn
         (with ,minx = (min ,(car x1.y1) ,(car x2.y2)))
         (with ,miny = (min ,(cdr x1.y1) ,(cdr x2.y2)))
         (with ,maxx = (max ,(car x1.y1) ,(car x2.y2)))
         (with ,maxy = (max ,(cdr x1.y1) ,(cdr x2.y2)))
         (with ,area = (* (1+ (- ,maxx ,minx)) (1+ (- ,maxy ,miny))))
         (with ,indices = (shuffle (iota (1- ,area) :start 0)))
         (with ,i = -1)
         (,kwd ,x.y next (progn
                           (incf ,i)
                           (if (>= ,i ,area) (terminate))
                           (area-index->coords (elt ,indices ,i)
                                               ,minx ,miny ,maxx ,maxy)))))))



(defmacro do-borders ((x y x1 y1 x2 y2 &key (corners? t)) &body body)
  "Given 2d array bounded by X1,Y1 --> X2,Y2.
Iterate through only the border points in this array, setting the
placeholders X and Y to the coordinates of each such point in turn.

If CORNERS? is nil, skip the four corner points."
  (once-only (x1 y1 x2 y2 corners?)
    (with-gensyms (do-borders-body minx miny maxx maxy)
      `(let ((,minx (min ,x1 ,x2)) (,miny (min ,y1 ,y2))
             (,maxx (max ,x1 ,x2)) (,maxy (max ,y1 ,y2)))
         (flet ((,do-borders-body (,x ,y)
                  ,@body))
           (loop for ,x from ,minx upto ,maxx
                 do (if (or (= ,x ,minx)
                            (= ,x ,maxx))
                        (loop for ,y from (if ,corners? ,miny (1+ ,miny))
                                upto (if ,corners? ,maxy (1- ,maxy))
                              do (,do-borders-body ,x ,y))
                        ;; else
                        (dolist (,y (list ,miny ,maxy))
                          (,do-borders-body ,x ,y)))))))))


(defmacro do-array-borders ((element xvar yvar array2d &key (corners? t))
                            &body body)
  "Given 2d array ARRAY2D, iterate through only the border points in the
array, setting the placeholders XVAR and YVAR to the coordinates of each
such point in turn, and the placeholder ELEMENT to the point as a setf-able
location.
If CORNERS? is nil, skip the four corner points."
  (once-only (array2d)
    `(do-borders (,xvar ,yvar 0 0 (array-xdim ,array2d) (array-ydim ,array2d)
                        :corners? ,corners?)
       (symbol-macrolet ((,element (aref ,array2d ,yvar ,xvar)))
         ,@body))))


(defmacro do-borders-no-corners ((x y x1 y1 x2 y2) &body body)
  "Given a 2d array bounded by X1,Y1 --> X2,Y2.
    Iterate through only the border points in this area, EXCLUDING the 4
    corner points. The placeholders X and Y are set to the coordinates of
    each point in turn."
  `(do-borders (,x ,y ,x1 ,y1 ,x2 ,y2 :corners? nil)
       ,@body))



(defun* (area-coords->index -> fixnum) ((x =coord=) (y =coord=)
                                        (x1 =coord=) (y1 =coord=)
                                        (x2 =coord=) (y2 =coord=))
  "Given an area with corners X1,Y1 --> X2,Y2. Number every position in the
area, starting in the corner with lowest X and Y coords, and moving along
each row (X). The first position is numbered 0. Return the number of the
position whose coordinates are X,Y."
  (+ (- x (min x1 x2)) (* (- y (min y1 y2))
                          (1+ (abs (- x1 x2))))))


(defun* (area-index->x -> =coord=) ((index fixnum)
                                    (x1 =coord=) (y1 =coord=)
                                    (x2 =coord=) (y2 =coord=))
  "Given an area with corners X1,Y1 --> X2,Y2. Number every position in the
area, starting in the top left corner and moving right along each row (X). The
first position is numbered 0. Return the X coordinate of the position numbered
INDEX."
  (declare (ignore y1 y2))
  (+ (min x1 x2) (mod index (1+ (abs (- x1 x2))))))


(defun* (area-index->y -> =coord=) ((index fixnum)
                                    (x1 =coord=) (y1 =coord=)
                                    (x2 =coord=) (y2 =coord=))
  "Given an area with corners X1,Y1 --> X2,Y2. Number every position in the
area, starting in the corner with lowest X and Y coords, and moving along each
row (X). The first position is numbered 0. Return the Y coordinate of the
position numbered INDEX."
  (+ (min y1 y2) (floor index (1+ (abs (- x1 x2))))))


(defun* (area-index->coords -> =coord-pair=) ((index fixnum)
                                              (x1 =coord=) (y1 =coord=)
                                              (x2 =coord=) (y2 =coord=))
  "Given an area with corners X1,Y1 --> X2,Y2. Number every position in the
area, starting in the corner with lowest X and Y coords, and moving along each
row (X). The first position is numbered 0. Return the (X . Y) coordinates of
the position numbered INDEX."
  (cons (area-index->x index x1 y1 x2 y2)
        (area-index->y index x1 y1 x2 y2)))


(defun* (random-coordinates -> =coord-pair=) ((x1 =coord=) (y1 =coord=)
                                              (x2 =coord=) (y2 =coord=))
  (cons (rand-between x1 x2) (rand-between y1 y2)))



(defun* (random-edge-coordinates -> =coord-pair=) ((x1 =coord=) (y1 =coord=)
                                                   (x2 =coord=) (y2 =coord=)
                                                   &key (allow-corners t))
  "X1,Y1 -> X2,Y2 define the boundaries of a rectangle (inclusive).
Return a random pair of coordinates on the edge of the rectangle.
If ALLOW-CORNERS is nil, do not ever return a coordinate that is
one of the 4 corners of the rectangle."
  (let ((x (if allow-corners
               (rand-between x1 x2)
               (rand-between (1+ x1) (1- x2))))
        (y (if allow-corners
               (rand-between y1 y2)
               (rand-between (1+ y1) (1- y2)))))
    (case (random 4)
      (0 (setf x x1))
      (1 (setf y y1))
      (2 (setf x x2))
      (3 (setf y y2)))
    (cons x y)))



;;;; <<2D Arrays>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defmacro do-2d-array ((element a xvar yvar &key (from-end nil)) &body body)
  "Where A is a 2-dimensional array (such as is created with (MAKE-ARRAY
'(ROWS COLS)) or (MAKE-2D-ARRAY ROWS COLS), iterate through every element
within the array. In the body of the macro, which is an implicit PROGN,
the variable name given for ELEMENT expands to (AREF A ROW COL) and so can
be used to retrieve or store a value. The variable names given for
XVAR and YVAR hold the column and row of the current element.
Example usage:
;;; (do-2d-array (elt (make-array '(3 5)) x y)
;;    (setf elt (random 10))
;;    (format t ''(~S,~S)  ~S~%'' x y elt))"
  (once-only (a)
    `(symbol-macrolet ((,element (aref ,a ,yvar ,xvar)))
       (cond
         (,from-end
          (loop for ,yvar from (1- (array-dimension ,a 0)) downto 0
                do (loop for ,xvar from (1- (array-dimension ,a 1)) downto 0
                         do ,@body)))
         (t
          (loop for ,yvar from 0 below (array-dimension ,a 0)
                do (loop for ,xvar from 0 below (array-dimension ,a 1)
                         do ,@body)))))))


(defun* (make-2d-array -> =2d-array=) ((xdim fixnum) (ydim fixnum)
                                       &key (initial-element nil)
                                            (initial-function nil))
  "Returns an XDIM * YDIM two-dimensional array. Initialises each element
by evaluating the expression supplied after :INITIAL-ELEMENT, or initialises to
NIL if not supplied.
If `initial-function' is supplied, it must be a function of 2 arguments. It
will be called for each (X, Y) position in the array, and its return value
will be used to initialise that position."
  (let ((newarray (make-array (list ydim xdim) :initial-element initial-element)))
    (when initial-function
      (do-2d-array (el newarray x y)
        (setf el (funcall initial-function x y))))
    newarray))


(defun* row-major-aref->x ((a =2d-array=) (n fixnum))
  "Assumes A is a 2-D array of dimensions '(Y X), where columns
are X coordinates and each row is a Y coordinate."
  (mod n (array-dimension a 1)))


(defun* row-major-aref->y ((a =2d-array=) (n fixnum))
  (floor n (array-dimension a 1)))


(defun* coords->row-major-aref ((a =2d-array=) (x fixnum) (y fixnum))
  (+ x (* y (array-dimension a 1))))


;; TODO
(defmacro do-2d-array-randomly ((element a xvar yvar)
                                &body body)
  "As for DO-2D-ARRAY, but iterates through elements in random order.
Not completely random: does the rows in random order, and for each row does
the columns in random order."
  (once-only (a)                 ; Don't re-evaluate `a' every time it appears.
    (with-gensyms (yvalues)
      `(symbol-macrolet ((,element (aref ,a ,yvar ,xvar)))
         (let ((,yvalues (shuffle
                          (loop for ,yvar from 0
                                  below (array-dimension ,a 0)
                                collect ,yvar))))
           (loop for ,yvar in ,yvalues
                 do (loop for ,xvar in (shuffle
                                        (loop for %x from 0
                                                below (array-dimension ,a 1)
                                              collect %x))
                          do ,@body)))))))



(defun* find-non-empty-row-in-2d-array ((a =2d-array=) &key (from-end nil))
  (do-2d-array (item a x y :from-end from-end)
    (when item
      (return-from find-non-empty-row-in-2d-array y)))
  nil)


(defun* find-non-empty-column-in-2d-array ((a =2d-array=) &key (from-end nil))
  (do-2d-array (item a x y :from-end from-end)
    (when item
      (return-from find-non-empty-column-in-2d-array x)))
  nil)


(defun* (array-xdim -> fixnum) ((a =2d-array=))
  (array-dimension a 1))


(defun* (array-ydim -> fixnum) ((a =2d-array=))
  (array-dimension a 0))


(defun* (out-of-2d-array-bounds? -> boolean) ((a =2d-array=)
                                              (x fixnum) (y fixnum))
  (not (and (<= 0 x (1- (array-xdim a)))
            (<= 0 y (1- (array-ydim a))))))


(defun* (on-array-border? -> boolean) ((a =2d-array=)
                                       (x fixnum) (y fixnum))
  (or (= x 0)
      (= y 0)
      (= x (1- (array-xdim a)))
      (= y (1- (array-ydim a)))))


(defun* array-at ((a =2d-array=) (x fixnum) (y fixnum))
  (aref a y x))


(defun* (setf array-at) (value (a =2d-array=) (x fixnum) (y fixnum))
  (setf (aref a y x) value))


(defun* (adjacent-array-coords -> list) ((a =2d-array=) (x fixnum) (y fixnum))
  "Return list of all coords adjacent to (X . Y) in array A.
Does not return the point itself."
  (let ((coords nil))
    (iterate
      (for px from (1- x) to (1+ x))
      (iterate
        (for py from (1- y) to (1+ y))
        (unless (or (out-of-2d-array-bounds? a px py)
                    (and (= px x) (= py y)))
          (push (cons px py) coords))))
    coords))


(defmacro-clause (FOR var :IN-ADJACENT-COORDS coords :OF-ARRAY a)
  "Usage:
: (for tile in-adjacent-array-coords (x y) of-array a) ...
Where x and y can be expressions."
  (destructuring-bind (x y) coords
    `(for ,var in (adjacent-array-coords ,a ,x ,y))))



(defmacro do-for-adjacent-coords ((a iterx itery x y) &body body)
  (once-only (a x y)
    `(loop for ,iterx from (1- ,x) upto (1+ ,x)
        do (loop for ,itery from (1- ,y) upto (1+ ,y)
              do (unless (or (out-of-2d-array-bounds? ,a ,iterx ,itery)
                             (and (= ,iterx ,x) (= ,itery ,y)))
                   ,@body)))))




(defun* print-2d-array ((a =2d-array=) &key value-print-fn coord-print-fn)
  "* Arguments
- A :: A two-dimensional array.
- VALUE-PRINT-FN :: A function which takes one value, and returns a
  string or other object which will be printed to represent that
  value.
- COORD-PRINT-FN :: Like VALUE-PRINT-FN, but takes two values,
  representing X and Y coordinates.
* Description
Print out a representation of the array to =*standard-output*=."
  (terpri)
  (do-2d-array (el a x y)
    (format t "~A"
            (cond
              (value-print-fn
               (funcall value-print-fn (aref a y x)))
              (coord-print-fn
               (funcall coord-print-fn x y))
              (t
               (left-elts (format nil "~A"
                                  (aref a y x))
                          1))))
    (if (>= x (1- (array-dimension a 1)))
        (terpri))))


(defun* (flip-2d-array-horizontal -> simple-array) ((a simple-array))
  (let ((newa (make-2d-array (array-xdim a) (array-ydim a))))
    (do-2d-array (el newa x y)
      (setf el (array-at a (- (array-xdim a) x 1) y)))
    newa))


(defun* (flip-2d-array-vertical -> simple-array) ((a simple-array))
  (let ((newa (make-2d-array (array-xdim a) (array-ydim a))))
    (do-2d-array (el newa x y)
      (setf el (array-at a x (- (array-ydim a) y 1))))
    newa))


(defun* (rotate-2d-array -> simple-array) ((a simple-array)
                                           (quarter-turns integer))
  "Rotate the 2D array A by the given number of quarter turns. A positive
number signifies clockwise rotation, a negative number anticlockwise."
  (let* ((turns (* (mod (abs quarter-turns) 4) (sign quarter-turns)))
         (newa (if (evenp turns)
                   (make-2d-array (array-xdim a) (array-ydim a))
                   (make-2d-array (array-ydim a) (array-xdim a))))
         (xdim (array-xdim a))
         (ydim (array-ydim a)))
    (do-2d-array (el a x y)
      (case turns
        ((1 -3) (setf (array-at newa (- ydim y 1) x) el))
        ((2 -2) (setf (array-at newa (- xdim x 1) (- ydim y 1)) el))
        ((3 -1) (setf (array-at newa y (- xdim x 1)) el))
        ((0 4) (setf (array-at newa x y) el))
        (otherwise (error "Invalid quarter-turns argument ~S" quarter-turns))))
    newa))


;;; <<Rectangle class>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Contentless polygon. Knows its coordinates and hence its area etc), but
;;; knows nothing about what may lie inside its bounds.

;;; '2d matrix' inherits from this class.


(defgeneric rect-xdim (rect))
(defgeneric rect-ydim (rect))
(defgeneric rect-area (rect))
(defgeneric rect-longest-axis (rect))
(defgeneric rect-centre-x (rect))
(defgeneric rect-centre-y (rect))
(defgeneric in-rectangle? (rect x y))
(defgeneric on-rectangle-border? (rect x y))
(defgeneric rect-overlaps-area? (rect xmin ymin xmax ymax))
(defgeneric rect-touches-area? (rect xmin ymin xmax ymax))
(defgeneric rectangles-touch? (rect1 rect2))
(defgeneric rectangles-overlap? (rect1 rect2))
(defgeneric rectangle-contains-rectangle? (inner outer))
(defgeneric rectangle-can-contain-rectangle? (inner outer))
(defgeneric inner-rectangle (rect &optional margin))
(defgeneric random-point-in-rectangle (rect &key test)
  (:documentation
   "Return X and Y coordinates of a random point within the rectangle
RECT. If TEST is supplied it must be a function that takes 2 arguments
and returns a boolean. Only a point where (TEST X Y) is true will
be returned."))
(defgeneric rotate-rectangle (rect &key quarter-turns)
  (:documentation
   "Rotate rectangle about its top left corner (MINX,MINY). The argument
QUARTER-TURNS specifies how many quarter turns the rectangle should be
rotated. A positive number is clockwise, negative is counterclockwise.
By default, the rectangle is rotated 90 degrees clockwise (+1)."))
(defgeneric move-rectangle (rect &key minx miny deltax deltay)
  (:documentation
   "Displace the rectangle rect. If MINX and/or MINY are given, move
the rectangle so its top left corner is positioned at those absolute
coordinates. If DELTAX and/or DELTAY are given, move the rectangle
by a relative amount."))
(defgeneric flip-rectangle-vertical (rect))
(defgeneric flip-rectangle-horizontal (rect))


(defclass rectangle ()
  ((rect-minx :initform 0 :type =coord= :initarg :minx :accessor rect-minx)
   (rect-miny :initform 0 :type =coord= :initarg :miny :accessor rect-miny)
   (rect-maxx :initform 0 :type =coord= :initarg :maxx :accessor rect-maxx)
   (rect-maxy :initform 0 :type =coord= :initarg :maxy :accessor rect-maxy)))


(defmethod initialize-instance :after ((rect rectangle)
                                       &key minx miny maxx maxy xdim ydim
                                       &allow-other-keys)
  (when xdim
    (cond
      ((and minx maxx)
       (error "You may specify EITHER dimensions OR coordinates for a ~
               new rectangle"))
      (minx
       (setf (rect-maxx rect) (+ minx xdim -1)))
      (maxx
       (setf (rect-minx rect) (- maxx xdim -1)))
      (t
       (setf (rect-minx rect) 0
             (rect-maxx rect) (1- xdim)))))
  (when ydim
    (cond
      ((and miny maxy)
       (error "You may specify EITHER dimensions OR coordinates for a ~
               new rectangle"))
      (miny
       (setf (rect-maxy rect) (+ miny ydim -1)))
      (maxy
       (setf (rect-miny rect) (- maxy ydim -1)))
      (t
       (setf (rect-miny rect) 0
             (rect-maxy rect) (1- ydim)))))
  (when (> (rect-miny rect) (rect-maxy rect))
    (psetf (rect-miny rect) (rect-maxy rect)
           (rect-maxy rect) (rect-miny rect)))
  (when (> (rect-minx rect) (rect-maxx rect))
    (psetf (rect-minx rect) (rect-maxx rect)
           (rect-maxx rect) (rect-minx rect))))


(defmethod print-object ((rect rectangle) strm)
  (print-unreadable-object (rect strm :type t :identity t)
    (slot-boundp rect 'rect-minx)
    (format strm "(~S,~S)-(~S,~S)"
            (if (slot-boundp rect 'rect-minx) (rect-minx rect) :unbound)
            (if (slot-boundp rect 'rect-miny) (rect-miny rect) :unbound)
            (if (slot-boundp rect 'rect-maxx) (rect-maxx rect) :unbound)
            (if (slot-boundp rect 'rect-maxy) (rect-maxy rect) :unbound))))


(defmethod rect-xdim ((rect rectangle))
  (- (rect-maxx rect) (rect-minx rect) -1))


(defmethod rect-ydim ((rect rectangle))
  (- (rect-maxy rect) (rect-miny rect) -1))


(defmethod rect-area ((rect rectangle))
  (* (rect-xdim rect) (rect-ydim rect)))


(defmethod rect-longest-axis ((rect rectangle))
  (cond
    ((> (rect-xdim rect) (rect-ydim rect))
     :x)
    ((< (rect-xdim rect) (rect-ydim rect))
     :y)
    (t nil)))


(defmethod rect-centre-x ((rect rectangle))
  (round (+ (rect-minx rect) (rect-maxx rect)) 2))


(defmethod rect-centre-y ((rect rectangle))
  (round (+ (rect-miny rect) (rect-maxy rect)) 2))


(defmethod in-rectangle? ((rect rectangle) (x fixnum) (y fixnum))
  (coordinates-in-area? x y (rect-minx rect) (rect-miny rect)
                        (rect-maxx rect) (rect-maxy rect)))


(defmethod on-rectangle-border? ((rect rectangle) (x fixnum) (y fixnum))
  (coordinates-on-area-border? x y (rect-minx rect) (rect-miny rect)
                               (rect-maxx rect) (rect-maxy rect)))


(defun rectangles-at (rectangles x y)
  (remove-if-not (lambda (rect) (in-rectangle? rect x y))
                 rectangles))


(defmethod rect-overlaps-area? ((rect rectangle) minx miny maxx maxy)
  (areas-overlap? (rect-minx rect) (rect-miny rect)
                  (rect-maxx rect) (rect-maxy rect)
                  minx miny maxx maxy))


(defmethod rect-touches-area? ((rect rectangle) minx miny maxx maxy)
  (areas-touch? (rect-minx rect) (rect-miny rect)
                (rect-maxx rect) (rect-maxy rect)
                minx miny maxx maxy))


(defmethod rectangles-touch? ((rect1 rectangle) (rect2 rectangle))
  (areas-touch? (rect-minx rect1) (rect-miny rect1)
                (rect-maxx rect1) (rect-maxy rect1)
                (rect-minx rect2) (rect-miny rect2)
                (rect-maxx rect2) (rect-maxy rect2)))


(defmethod rectangles-overlap? ((rect1 rectangle) (rect2 rectangle))
  (areas-overlap? (rect-minx rect1) (rect-miny rect1)
                  (rect-maxx rect1) (rect-maxy rect1)
                  (rect-minx rect2) (rect-miny rect2)
                  (rect-maxx rect2) (rect-maxy rect2)))


(defmethod rectangle-contains-rectangle? ((inner rectangle) (outer rectangle))
  (area-contains-area? (rect-minx inner) (rect-miny inner)
                       (rect-maxx inner) (rect-maxy inner)
                       (rect-minx outer) (rect-miny outer)
                       (rect-maxx outer) (rect-maxy outer)))


(defmethod rectangle-can-contain-rectangle? ((inner rectangle)
                                             (outer rectangle))
  (and (<= (rect-xdim inner) (rect-xdim outer))
       (<= (rect-ydim inner) (rect-ydim outer))))


(defmethod move-rectangle ((rect rectangle) &key minx miny deltax deltay)
  (let ((xdim (rect-xdim rect))
        (ydim (rect-ydim rect)))
    (when (numberp minx)
      (setf (rect-minx rect) minx)
      (setf (rect-maxx rect) (+ minx xdim -1)))
    (when (numberp miny)
      (setf (rect-miny rect) miny)
      (setf (rect-maxy rect) (+ miny ydim -1)))
    (when (numberp deltax)
      (incf (rect-minx rect) deltax)
      (incf (rect-maxx rect) deltax))
    (when (numberp deltay)
      (incf (rect-miny rect) deltay)
      (incf (rect-maxy rect) deltay))
    rect))


(defmethod rotate-rectangle ((rect rectangle)
                             &key (quarter-turns +1))
  (case (* (mod (abs quarter-turns) 4) (sign quarter-turns))
    ((1 -3)
     (psetf (rect-minx rect) (- (rect-minx rect) (1- (rect-ydim rect)))
            (rect-maxx rect) (rect-minx rect)
            (rect-maxy rect) (+ (rect-miny rect) (1- (rect-xdim rect)))))
    ((2 -2)
     (psetf (rect-maxx rect) (rect-minx rect)
            (rect-maxy rect) (rect-miny rect)
            (rect-minx rect) (- (rect-minx rect) (1- (rect-xdim rect)))
            (rect-miny rect) (- (rect-miny rect) (1- (rect-ydim rect)))))
    ((3 -1)
     (psetf (rect-miny rect) (- (rect-miny rect) (1- (rect-xdim rect)))
            (rect-maxy rect) (rect-miny rect)
            (rect-maxx rect) (+ (rect-minx rect) (1- (rect-ydim rect)))))
    ((4 0)
     nil)
    (otherwise
     (error "Invalid quarter-turns argument: ~S" quarter-turns)))
  rect)


(defmethod flip-rectangle-horizontal ((rect rectangle))
  rect)

(defmethod flip-rectangle-vertical ((rect rectangle))
  rect)

(defmacro do-rectangle ((xvar yvar rect) &body body)
  (once-only (rect)
    `(do-area (,xvar ,yvar (rect-minx ,rect) (rect-miny ,rect)
                     (rect-maxx ,rect) (rect-maxy ,rect))
       ,@body)))


(defmacro do-rectangle-borders ((xvar yvar rect &key (corners? t)) &body body)
  (once-only (rect)
  `(do-borders (,xvar ,yvar (rect-minx ,rect) (rect-miny ,rect)
                (rect-maxx ,rect) (rect-maxy ,rect) :corners? ,corners?)
     ,@body)))


(defmethod inner-rectangle ((rect rectangle) &optional (margin 1))
  (assert (> (rect-xdim rect) (* 2 margin)))
  (assert (> (rect-ydim rect) (* 2 margin)))
  (make-instance 'rectangle
                 :minx (+ (rect-minx rect) margin)
                 :miny (+ (rect-miny rect) margin)
                 :maxx (- (rect-maxx rect) margin)
                 :maxy (- (rect-maxy rect) margin)))


(defmethod random-point-in-rectangle ((rect rectangle) &key test)
  (cond
    ((null test)
     (values (rand-between (rect-minx rect) (rect-maxx rect))
             (rand-between (rect-miny rect) (rect-maxy rect))))
    (t
     (iterate
       (for x = (rand-between (rect-minx rect) (rect-maxx rect)))
       (for y = (rand-between (rect-miny rect) (rect-maxy rect)))
       (until (funcall test x y))
       (finally
        (return (values x y)))))))


(defmacro with-random-point-in-rectangle ((xvar yvar rect &key test) &body body)
  `(multiple-value-bind (,xvar ,yvar)
       (random-point-in-rectangle ,rect :test ,test)
     ,@body))


(defun print-rectangles (rects &key (coord-test nil) (coord-printer nil)
                               (list-rectangles? nil) (start-at-origin? nil))
  "COORD-TEST : if this is given, it must be a predicate that takes X and Y
arguments. It should return true if the point X,Y should be printed by
COORD-PRINTER rather than by PRINT-RECTANGLES."
  (let ((minx (if start-at-origin? 0 (rect-minx (least #'rect-minx rects))))
        (miny (if start-at-origin? 0 (rect-miny (least #'rect-miny rects))))
        (maxx (rect-maxx (greatest #'rect-maxx rects)))
        (maxy (rect-maxy (greatest #'rect-maxy rects))))
    (flet ((default-coord-printer (x y)
             (declare (ignorable x y))
             " "))
      (iterate
        (for y from miny to maxy)
        (iterate
          (with match = nil)
          (for x from minx to maxx)
          (cond
            ((and coord-test
                  (funcall coord-test x y))
             (format t "~A" (funcall (or coord-printer #'default-coord-printer)
                                     x y)))
            (t
             (let ((rects-here (rectangles-at rects x y)))
               (setf match (if rects-here (least #'rect-area rects-here) nil)))
             (format t "~C"
                     (code-char
                      (clamp (+ 32 (if match
                                       (1+ (position match rects))
                                       0))
                             32 126)))))
          (finally (fresh-line))))
      (when list-rectangles?
        (iterate
          (for rect in rects)
          (format t "\"~C\" = rectangle: ~A~%"
                  (code-char (clamp (+ 32 (if rect
                                              (1+ (position rect rects))
                                              0))
                                    32 126))
                  rect))))
    rects))


;;; <<2D Matrix Class>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defgeneric matrix-value-at (matrix x y))
(defgeneric (setf matrix-value-at) (value matrix x y))
(defgeneric matrix-value->string (matrix x y))
(defgeneric matrix-blit (source dest src-x1 src-y1 src-x2 src-y2
                                dest-x1 dest-y1 dest-x2 dest-y2 &key))
(defgeneric print-matrix (matrix &key))
(defgeneric matrix-array (matrix))
(defgeneric matrix-xdim (matrix))
(defgeneric matrix-ydim (matrix))
(defgeneric random-matrix-value-in-rectangle (matrix rect))
(defgeneric map-matrix (matrix fn))


(defclass <2d-matrix> (rectangle)
  ((matrix-matrix :initform nil :documentation "2D array of values"
                  :accessor matrix-matrix)
   (matrix-initial-element
    :initform nil :initarg :initial-element
    :accessor matrix-initial-element)
   (matrix-initial-function
    :initform nil :initarg :initial-function
    :accessor matrix-initial-function
    :documentation "If supplied, must be a function that accepts two arguments.
It will be called for each (X, Y) position in the matrix, and its return value
will be used to initialise that position.")))

(defmethod matrix-xdim ((matrix <2d-matrix>)) (rect-xdim matrix))
(defmethod matrix-ydim ((matrix <2d-matrix>)) (rect-ydim matrix))

(defmethod initialize-instance :after ((matrix <2d-matrix>) &key)
  (setf (matrix-matrix matrix)
        (make-2d-array (matrix-xdim matrix) (matrix-ydim matrix)
                       :initial-element (matrix-initial-element matrix)
                       :initial-function (matrix-initial-function matrix))))


(defmethod matrix-array ((matrix <2d-matrix>))
  (matrix-matrix matrix))


(defmacro do-matrix ((element xvar yvar matrix) &body body)
  (once-only (matrix)
    `(do-rectangle (,xvar ,yvar ,matrix)
       (symbol-macrolet ((,element (matrix-value-at ,matrix ,xvar ,yvar)))
         ,@body))))


(defmethod matrix-value-at ((matrix <2d-matrix>) (x fixnum) (y fixnum))
  "Return the value at position X,Y."
  (if (in-rectangle? matrix x y)
      (aref (matrix-matrix matrix)
            (- y (rect-miny matrix))
            (- x (rect-minx matrix)))
      nil))


(defmethod (setf matrix-value-at) (value (matrix <2d-matrix>)
                                   (x fixnum) (y fixnum))
  "Set the value at position X,Y."
  (if (in-rectangle? matrix x y)
      (setf (aref (matrix-matrix matrix)
                  (- y (rect-miny matrix))
                  (- x (rect-minx matrix)))
            value)
      nil))


(defmethod matrix-value->string ((matrix <2d-matrix>) (x fixnum) (y fixnum))
  (left-elts (format nil "~A" (matrix-value-at matrix x y)) 1))


(defmethod rotate-rectangle :before ((matrix <2d-matrix>)
                                     &key (quarter-turns 1))
  (setf (matrix-matrix matrix)
        (rotate-2d-array (matrix-matrix matrix) quarter-turns)))


(defmethod flip-rectangle-vertical :before ((matrix <2d-matrix>))
  (setf (matrix-matrix matrix)
        (flip-2d-array-vertical (matrix-matrix matrix))))


(defmethod flip-rectangle-horizontal :before ((matrix <2d-matrix>))
  (setf (matrix-matrix matrix)
        (flip-2d-array-horizontal (matrix-matrix matrix))))


(defmethod matrix-blit ((source <2d-matrix>) (dest <2d-matrix>)
                        (src-x1 fixnum) (src-y1 fixnum)
                        (src-x2 fixnum) (src-y2 fixnum)
                        (dest-x1 fixnum) (dest-y1 fixnum)
                        (dest-x2 fixnum) (dest-y2 fixnum)
                        &key (map-function #'identity)
                             (cell-function nil)
                             (use-matrix nil))
  "For the rectangular area SRC-X1,SRC-Y1 -> SRC-X2,SRC-Y2 in the matrix
SOURCE, copy each cell's value into the area DEST-X1,DEST-Y1 -> DEST-X2,DEST-Y2
in the matrix DEST.
If MAP-FUNCTION is supplied, the value copied into each DEST cell will be the
result of MAP-FUNCTION with each SOURCE cell value as an argument.
If CELL-FUNCTION is supplied, it must be a function that takes three arguments,
a tile and X and Y coordinates. CELL-FUNCTION will be called for each cell
with the cell's new coordinates as arguments."
  (let* ((src-maxx (max src-x1 src-x2)) (src-maxy (max src-y1 src-y2))
         (src-minx (min src-x1 src-x2)) (src-miny (min src-y1 src-y2))
         (dest-maxx (max dest-x1 dest-x2)) (dest-maxy (max dest-y1 dest-y2))
         (dest-minx (min dest-x1 dest-x2)) (dest-miny (min dest-y1 dest-y2))
         (xdim (- (1+ src-maxx) src-minx))
         (ydim (- (1+ src-maxy) src-miny))
         (tmp-matrix (or use-matrix
                         (make-instance '<2d-matrix>
                                        :xdim xdim
                                        :ydim ydim))))
    (declare (ignore dest-maxx dest-maxy))
    ;; TODO: just copy a->b if matrices not the same
    (iterate
      (for x from src-minx to src-maxx)
      (iterate
        (for y from src-miny to src-maxy)
        (when (in-rectangle? source x y)
          (setf (matrix-value-at tmp-matrix (- x src-minx) (- y src-miny))
                (funcall map-function (matrix-value-at source x y))))))
    (iterate
      (for x from 0 below xdim)
      (iterate
        (for y from 0 below ydim)
        (when (in-rectangle? dest (+ x dest-minx) (+ y dest-miny))
          (let ((cell (matrix-value-at tmp-matrix x y)))
            (setf (matrix-value-at dest (+ x dest-minx) (+ y dest-miny))
                  cell)
            (if cell-function
                (funcall cell-function cell (+ x dest-minx) (+ y dest-miny)))))))))





(defmethod print-matrix ((matrix <2d-matrix>) &key printer)
  "Print out a representation of the matrix to the console.
PRINTER, if supplied, is a function which takes one argument (the value stored
in a matrix cell) and returns a value which will be printed to represent that
cell. The value returned should usually be a character or length-1 string."
  (terpri)
  (iterate
    (for y from (rect-miny matrix) to (rect-maxy matrix))
    (iterate
      (for x from (rect-minx matrix) to (rect-maxx matrix))
      (format t "~A" (if printer
                         (funcall printer (matrix-value-at
                                           matrix x y))
                         (matrix-value->string matrix x y)))
      (finally (fresh-line)))))


(defclass <char-matrix> (<2d-matrix>)
  ((matrix-initial-element :initform #\Space)))


(defmethod matrix-value->string ((matrix <char-matrix>)
                                 (x integer) (y integer))
  (format nil "~A" (matrix-value-at matrix x y)))


(defmethod random-matrix-value-in-rectangle ((matrix <2d-matrix>)
                                             (rect rectangle))
  (multiple-value-bind (x y) (random-point-in-rectangle rect)
    (matrix-value-at matrix x y)))


(defmethod map-matrix ((matrix <2d-matrix>) fn)
  (let ((new (shallow-copy-object matrix)))
    (setf (matrix-matrix new)
          (map-array fn (matrix-matrix matrix)))
    new))


;;;; <<Lists of coordinates>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun* coord-sort ((pair1 =coord-pair=) (pair2 =coord-pair=)
                    &optional (predicate #'<))
  "Utility function used to sort coordinates, which are cons pairs of the
form (X . Y). The default predicate #'< sorts them according to their
nearness to 0,0.
The idea is this can be used as the predicate for SORT."
  (if (eq (car pair1) (car pair2))
      (funcall predicate (cdr pair1) (cdr pair2))
      (funcall predicate (car pair2) (car pair2))))


(defun* (group-coords -> list) ((coords list))
  "COORDS is a list of (X . Y) coordinates. Returns a list of lists of
the coordinates, grouped into sets of coordinates whose members are touching
horizontally or vertically."
  (cond
    ((null coords)
     nil)
    ((null (second coords))
     (list (list (first coords))))
    (t
     (let ((group (find-coords-touching (car coords) (cdr coords))))
       (if-let (diff (set-difference coords group))
               (cons group (group-coords diff))
               (list group))))))


(defun* (find-coords-touching -> list) ((pair =coord-pair=) (others list))
  (let ((touching (remove-if-not
                   (lambda (other)
                     (or (and (= 1 (abs (- (car pair) (car other))))
                              (= (cdr pair) (cdr other)))
                         (and (= 1 (abs (- (cdr pair) (cdr other))))
                              (= (car pair) (car other)))))
                   others)))
    (cond
      (touching
       (remove-duplicates
        (cons pair (apply #'append
                          (mapcar #'(lambda (coords)
                                      (find-coords-touching
                                       coords
                                       (set-difference others touching)))
                                  touching)))
        :test #'equal))
      (t
       (list pair)))))



(defun* (coord-groups-in-line? -> boolean) ((group1 list) (group2 list))
  "GROUP1 and GROUP2 are lists of (X . Y) coordinates.
Returns true if the two groups 'line up' horizontally or vertically."
  (if (and (and group1 group2)
           (or (intersection (mapcar #'cdr group1) (mapcar #'cdr group2))
               (intersection (mapcar #'car group1) (mapcar #'car group2))))
      t nil))



(define-memo-function coords-at-distance (d)
  "Return list of all coordinates that are at distance D from the origin (0,0).
Each coordinate is returned in the form of a cons cell (X . Y)"
  (declare (integer d))
  (let ((coords nil))
    (iterate
      (for x from (- d) to (+ d))
      (iterate
        (for y from (- d) to (+ d))
        (if (= (dist 0 0 x y) d)
            (push (cons x y) coords))))
    coords))


(define-memo-function coords-within-distance (d)
  "Return list of all coordinates that are at or closer than distance D
from the origin (0,0). Each coordinate is returned in the form of a
cons cell (X . Y)"
  (declare (integer d))
  (let ((coords nil))
    (iterate
      (for x from (- d) to (+ d))
      (iterate
        (for y from (- d) to (+ d))
        (if (<= (dist 0 0 x y) d)
            (push (cons x y) coords))))
    coords))


(defun* print-coords ((coords list)
                      &key (minx (car (least #'car coords)))
                           (miny (cdr (least #'cdr coords)))
                           (maxx (car (greatest #'car coords)))
                           (maxy (cdr (greatest #'cdr coords))))
  "COORDS is a list of (X . Y) cons pairs."
  (flet ((print-coord (x y)
           (format t "~C"
                   (cond
                     ((find (cons x y) coords :test 'equal)
                      #\X)
                     ((or (< x 0) (< y 0))
                      #\-)
                     (t
                      #\.)))
           (when (= x maxx)
             (format t "~%"))))
    (cond
      (*invert-y-axis?*
       (iterate
         (for y from miny to maxy)
         (format t "~10D: " y)
         (iterate
           (for x from minx to maxx)
           (print-coord x y))))
      (t
       (iterate
         (for y from maxy downto miny)
         (format t "~10D: " y)
         (iterate
           (for x from minx to maxx)
           (print-coord x y)))))))


;;;; <<Iteration through coordinates>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; (defmacro do-radius-around-point ((iterx itery pointx pointy
;;                                          &optional (radius 1))
;;                                   &body body)
;;   "Loop through every point within RADIUS squares of the point POINTX, POINTY.
;; For each point, ITERX is bound to the x-coordinate, and ITERY to
;; the y-coordinate."
;;   (once-only (radius pointx pointy)
;;     `(loop for ,iterx from (- ,pointx ,radius) upto (+ ,pointx ,radius)
;;         do (loop for ,itery from (- ,pointy ,radius) upto (+ ,pointy ,radius)
;;               do (unless (> (dist ,iterx ,itery ,pointx ,pointy) ,radius)
;;                    ,@body)))))


(defmacro-driver (FOR coords :IN-COORDS-WITHIN distance
                             :OF origin)
  "Iterate through all (x,y) coordinate pairs that lie DISTANCE or less
distance from ORIGIN.
- COORDS: a cons cell of variable names (X . Y)
- DISTANCE: an expression that evaluates to an integer.
- ORIGIN: a cons cell of expressions that evaluate to X and Y coordinates.
Example usage:
;;; (iterate
;;;   (for (x . y) in-coords-within 5 of (10 . 10))
;;;   (format t ''~D, ~D~%'' x y))
"
  (let ((cc (gensym))
        (centrex (gensym))
        (centrey (gensym))
        (kwd (if generate 'generate 'for)))
    `(progn
       (with ,cc = (coords-within-distance ,distance))
       (with ,centrex = ,(car origin))
       (with ,centrey = ,(cdr origin))
       (,kwd ,coords next (let ((xy (pop ,cc)))
                            (cond
                              (xy (cons (+ (car xy) ,centrex)
                                        (+ (cdr xy) ,centrey)))
                              (t (terminate))))))))


(defmacro-clause (FOR coords :IN-COORDS-FROM-DISTANCE start-radius
                   :TO end-radius :AROUND centre)
  "Iterate through all coordinates that are between  distances START-RADIUS and
END-RADIUS measured from the point CENTRE. The distance starts equal to
START-RADIUS, and moves toward END-RADIUS. For each radius, the relevant points
are processed in a random order. Within BODY, the symbols in the cons cell
COORDS are bound to the X and Y coordinates of each tile."
  (with-gensyms (cx cy basecoords)
    `(progn
       (with ,cx = ,(car centre))
       (with ,cy = ,(cdr centre))
       (for ,basecoords in (apply #'append
                                  (mapcar #'shuffle
                                          (mapcar #'coords-at-distance
                                                  (range ,start-radius
                                                         ,end-radius)))))
       (for ,(car coords) = (+ (car ,basecoords) ,cx))
       (for ,(cdr coords) = (+ (cdr ,basecoords) ,cy)))))


(defmacro-clause (FOR coords :IN-COORDS-OUT-TO-DISTANCE distance :FROM centre)
  `(for ,coords :in-coords-from-distance 0 :to ,distance
                :around ,centre))


(defmacro-clause (FOR coords :IN-COORDS-IN-FROM-DISTANCE distance :TO centre)
  `(for ,coords :in-coords-from-distance ,distance :to 0
                :around ,centre))


(defmacro-clause (FOR coords :IN-COORDS-AT-DISTANCE distance :FROM centre)
  `(for ,coords :in-coords-from-distance ,distance :to ,distance
                :around ,centre))


;;;; <<Directions>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defun* (dir->dx -> =coord=) ((dir =direction=))
  (aref #(0 -1 0 1 -1 0 1 -1 0 1) dir))

(defun* (dir->dy -> =coord=) ((dir =direction=))
  (* (if *invert-y-axis?* -1 1)
     (aref #(0 -1 -1 -1 0 0 0 1 1 1) dir)))


(defun* (coords->dir -> (or null =direction=))
    ((dx =coord=) (dy =coord=)
     &key ((allow-diagonals? boolean) t))
  (let ((deg (radians->degrees
              (polar-rho dx (if *invert-y-axis?* (- dy) dy)))))
    (cond
      (allow-diagonals?
       (cond
         ((degrees-in-range? deg -23 22)
          6)
         ((degrees-in-range? deg  22 68)
          9)
         ((degrees-in-range? deg  68 114)
          8)
         ((degrees-in-range? deg  114 149)
          7)
         ((degrees-in-range? deg  149 194)
          4)
         ((degrees-in-range? deg  194 239)
          1)
         ((degrees-in-range? deg  239 284)
          2)
         (t
          3)))
      (t
       (cond
         ((degrees-in-range? deg -45 45)
          6)
         ((degrees-in-range? deg 45 135)
          8)
         ((degrees-in-range? deg 135 225)
          4)
         (t
          2))))))

;; normal axis
;; + 789
;;   456
;; - 123

(defun* (dx-dy->dir -> (or null =direction=))
    ((dx =coord=) (dy =coord=))
  (if *invert-y-axis?* (setf dy (- dy)))
  (cond
    ((minusp dx)
     (cond
       ((plusp dy) 7)
       ((zerop dy) 4)
       (t 1)))
    ((zerop dx)
     (cond
       ((plusp dy) 8)
       ((zerop dy) 5)
       (t 2)))
    (t                                  ; plusp
     (cond
       ((plusp dy) 9)
       ((zerop dy) 6)
       (t 3)))))


(defun* (opposite-dir -> =direction=) ((dir =direction=))
    "Returns the direction that is 180 degrees opposite to DIR."
    (- 10 dir))

(defun* (x+dir -> =coord=) ((x =coord=) (dir =direction=)
                            &optional ((steps real) 1))
  "Return X + DX"
  (+ x (* steps (dir->dx dir))))


(defun* (y+dir -> =coord=) ((y =coord=) (dir =direction=)
                            &optional ((steps real) 1))
  "Return Y + DY"
  (+ y (* steps (dir->dy dir))))


(defun* (left-dir -> =direction=) ((dir =direction=))
  "Return the direction that is 90 degrees to the left of DIR."
  (aref #(0 3 6 9 2 5 8 1 4 7) dir))


(defun* (right-dir -> =direction=) ((dir =direction=))
  "Return the direction that is 90 degrees to the left of DIR."
  (aref #(0 7 4 1 8 5 2 9 6 3) dir))


(defun* (soft-left-dir -> =direction=) ((dir =direction=))
  "Return the direction that is 45 degrees to the left of DIR."
  (aref #(0 2 3 6 1 5 9 4 7 8) dir))


(defun* (soft-right-dir -> =direction=) ((dir =direction=))
  "Return the direction that is 45 degrees to the left of DIR."
  (aref #(0 4 1 2 7 5 3 8 9 6) dir))


(defun* (hard-left-dir -> =direction=) ((dir =direction=))
  "Return the direction that is 135 degrees to the left of DIR."
  (aref #(0 6 9 8 3 0 7 2 1 4) dir))


(defun* (hard-right-dir -> =direction=) ((dir =direction=))
  "Return the direction that is 135 degrees to the right of DIR."
  (aref #(0 8 7 4 9 0 1 6 3 2) dir))


(defun* (dir->degrees -> (or null =degrees=)) ((dir =direction=))
  "Return DIR as degrees of rotation in an anticlockwise direction from east
  (this is for compatibility with the polar coordinate system)"
  (case dir
    (6 0)
    (9 45)
    (8 90)
    (7 135)
    (4 180)
    (1 225)
    (2 270)
    (3 315)
    (otherwise nil)))


(defun* (dir->vector -> (or null =coord-pair=)) ((dir =direction=))
  "* Arguments:
- DIR: A direction (1-9).
* Returns:
- A vector (cons pair of coordinates) corresponding to the given direction,
on a coordinate system whose origin is at top left."
  (cons (dir->dx dir) (dir->dy dir)))


(defun* (dir->short-compass-direction -> string) ((dir =direction=))
  (aref #("0"
          "SW" "S" "SE"
          "W" "5" "E"
          "NW" "N" "NE") dir))

(defun* (dir->long-compass-direction -> string) ((dir =direction=))
  (aref #("0"
          "southwest" "south" "southeast"
          "west" "5" "east"
          "northwest" "north" "northeast") dir))


(defun random-direction ()
  "Return a random keypad direction"
  (nth (random 8) '(1 2 3 4 6 7 8 9)))


(defun* (dir->corner -> (values =coord= =coord=)) ((dir =direction=)
                                                   (x1 =coord=) (y1 =coord=)
                                                   (x2 =coord=) (y2 =coord=))
  "* Returns: Two values:
- X coordinate of corner
- Y coordinate of corner"
  (let ((x1/x2 (+ x1 (/ (- x2 x1) 2)))
        (y1/y2 (+ y1 (/ (- y2 y1) 2))))
    (values (aref (vector 0
                          x1 x1/x2 x2
                          x1 x1/x2 x2
                          x1 x1/x2 x2)
                  dir)
            (aref (vector 0
                          y2 y2 y2
                          y1/y2 y1/y2 y1/y2
                          y1 y1 y1)
                  dir))))


(define-memo-function cartesian-coordinates->dir (x y)
  "Return the direction 1-9 that most closely approximates THETA for the
vector (0,0)->(x,y). X and Y are true cartesian coordinates i.e. positive
is up for Y and rightward for X."
  (cond
    ((= 0 x y)
     nil)
    (t
     (let ((vecdeg (radians->degrees (polar-theta x y))))
       (dolist (deg '((9 22.5 67.5)
                      (8 67.5 112.5)
                      (7 112.5 157.5)
                      (4 157.5 202.5)
                      (1 202.5 247.5)
                      (2 247.5 292.5)
                      (3 292.5 337.5)
                      (6 337.5 22.5)))
         (if (degrees-in-range? vecdeg
                                (second deg) (third deg))
             (return-from cartesian-coordinates->dir (first deg))))
       nil))))




(defun* (dir-diff -> (integer -3 4)) ((dir1 =direction=) (dir2 =direction=))
  "Return the number of 1/8th-circle turns necessary to change direction from
DIR1 to DIR2. A negative number implies turning to the left, a positive
number implies turning to the right."
  (cond
    ((= dir2 (soft-left-dir dir1))
     -1)
    ((= dir2 (soft-right-dir dir1))
     1)
    ((= dir2 (left-dir dir1))
     -2)
    ((= dir2 (right-dir dir1))
     2)
    ((= dir2 (hard-left-dir dir1))
     -3)
    ((= dir2 (hard-right-dir dir1))
     3)
    ((= dir2 (opposite-dir dir1))
     4)
    ((= dir1 5)
     1)
    (t
     0)))



;;; <<Polar coordinates>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Convert Cartesian to polar coordinates.
;;; Cartesian coordinates take the familiar form X, Y, where X is the
;;; horizontal and Y the vertical axis.
;;; Polar coordinates take the form rho, theta, where rho is the
;;; distance from the origin to the point, and theta is the angle,
;;; with zero degrees being in the direction of the positive X axis
;;; and increasing anticlockwise.
;;;
;;; To derive the rho for point X,Y:
;;;
;;;   rho = _________
;;;        /  2     2        i.e. sqrt(x^2 + y^2)
;;;       V  x  +  y
;;;
;;; To derive theta:
;;;
;;;   theta = atan(y, x)     with the result usually provided in radians.
;;;
;;; To convert polar coordinates back to cartesian:
;;;
;;;   x = rho * cos theta
;;;   y = rho * sin theta
;;;
;;; To convert radians to degrees:
;;;
;;;   degrees = 180
;;;            ----- * radians      i.e. (180/pi)*radians
;;;              pi
;;;
;;; We memoize these functions to increase their speed with repeated calls.



(define-memo-function polar-rho  (x y)
  "Return the polar coordinate RHO for the Cartesian coordinates X, Y."
  (sqrt (+ (* x x) (* y y))))


(define-memo-function polar-theta (x y)
  "Return the polar coordinate THETA for the Cartesian coordinates X, Y;
THETA is given in radians"
  (assert (not (= x y 0)) (x y)
          "Illegal to calculate POLAR-THETA at the origin.")
  (atan y x))


(define-memo-function polar->cartesian-x (rho theta)
  "Return the Cartesian coordinate X for the polar coordinates RHO, THETA."
  (* rho (cos theta)))


(define-memo-function polar->cartesian-y (rho theta)
  "Return the Cartesian coordinate Y for the polar coordinates RHO, THETA."
  (* rho (sin theta)))


;;;; <<Vectors>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun* (line->vector -> =2d-vector=) ((x1 =coord=) (y1 =coord=)
                                       (x2 =coord=) (y2 =coord=))
  "Given a line segment (x1,Y1) -> (x2,y2), return an equivalent vector."
  (cons (- x2 x1) (- y2 y1)))


(defun* (vector-normalise -> =2d-vector=) ((vec =2d-vector=)
                                           &optional ((norm real) 1.0))
  (let ((longest (max (abs (car vec)) (abs (cdr vec)))))
    (cond
      ((zerop longest)
       (cons 0 0))
      (t
       (cons (* norm (/ (car vec) longest))
             (* norm (/ (cdr vec) longest)))))))


(defun* (vector-right -> =2d-vector=) ((vec =2d-vector=))
  "Given a vector of relative coordinates X and Y, return the vector that
is perpendicular and moving to the right."
  (cons (cdr vec) (* -1 (car vec))))


(defun* (vector-left -> =2d-vector=) ((vec =2d-vector=))
  "Given a vector of relative coordinates X and Y, return the vector that
is perpendicular and moving to the left."
  (cons (* -1 (cdr vec)) (car vec)))


(defun* (vector* -> =2d-vector=) ((vec =2d-vector=) (factor real))
  (cons (* factor (car vec)) (* factor (cdr vec))))


(defun* (vector+ -> =2d-vector=) ((vec1 =2d-vector=) (vec2 =2d-vector=))
  (cons (+ (car vec1) (car vec2)) (+ (cdr vec1) (cdr vec2))))


(defun* (vector-reverse -> =2d-vector=) ((vec =2d-vector=))
  "Return a vector pointing in the opposite direction from VEC."
  (vector* vec -1))


(defun* (vector-flip-y-axis -> =2d-vector=) ((vec =2d-vector=))
  "Returns the vector with the Y coordinate multiplied by -1.
Useful to convert 'origin at top left' coordinates to the
classical Cartesian system."
  (cons (car vec) (- (cdr vec))))


(defun* (vector- -> =2d-vector=) ((vec1 =2d-vector=) (vec2 =2d-vector=))
  (vector+ vec1 (vector* vec2 -1)))


(defun* (vector-ceiling -> =2d-vector=) ((vec =2d-vector=))
  (cons (ceiling (car vec)) (ceiling (cdr vec))))


(defun* (vector/ -> =2d-vector=) ((vec =2d-vector=)
                                  (divisor real (/= 0 divisor)))
  (vector* vec (/ 1 divisor)))


(defun* (vector-angle -> =degrees=) ((x1 =coord=) (y1 =coord=)
                                     (x2 =coord=) (y2 =coord=))
  (round (radians->degrees (polar-theta (- x2 x1)
                                    (if *invert-y-axis?*
                                        (- (- y2 y1))
                                        (- y2 y1))))))


(defun* (vector-sign -> (cons =sign= =sign=)) ((vec =2d-vector=))
  (cons (sign (car vec)) (sign (cdr vec))))


(defun* (vector-gradient -> (or null real)) ((vec =2d-vector=))
  "Return NIL if the vector goes straight up, ie. if X component is zero."
  (if (zerop (car vec)) ;; infinite gradient
      nil
      (/ (cdr vec) (car vec))))


(defun* (dot-product -> real) ((vec1 =2d-vector=) (vec2 =2d-vector=))
  (+ (* (car vec1) (car vec2))
     (* (cdr vec1) (cdr vec2))))


(defun* (vector-length -> real) ((vec =2d-vector=))
  (dist 0 0 (car vec) (cdr vec)))


(defun* (cosine-between-vectors -> real) ((ux real) (uy real)
                                                 (vx real) (vy real))
  "Return the cosine of the angle theta between vectors (ux,uy) and (vx,vy)."
  (/ (+ (* ux vx) (* uy vy))
     (* (sqrt (+ (* ux ux) (* uy uy)))
        (sqrt (+ (* vx vx) (* vy vy))))))


(defun* (angle-between-vectors -> real) ((ux real) (uy real)
                                         (vx real) (vy real))
  "Return the cosine of the angle theta between vectors (ux,uy) and (vx,vy)."
  (acos (cosine-between-vectors ux uy vx vy)))



;;;; <<Lines>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun* (line-gradient -> (or null real)) ((x1 =coord=) (y1 =coord=)
                                           (x2 =coord=) (y2 =coord=))
  (vector-gradient (if (> x2 x1) (line->vector x1 y1 x2 y2)
                       (line->vector x2 y2 x1 y1))))


(defun* (line-y-intercept -> (or null real)) ((x1 =coord=) (y1 =coord=)
                                              (x2 =coord=) (y2 =coord=))
  (if-let (gradient (line-gradient x1 y1 x2 y2))
          (- y1 (* gradient x1))))


(defun* (line-mid-point -> (cons =coord= =coord=)) ((x1 =coord=) (y1 =coord=)
                                                    (x2 =coord=) (y2 =coord=))
  (cons (+ x1 (/ (- x2 x1) 2.0))
        (+ y1 (/ (- y2 y1) 2.0))))



(defun* (is-point-on-line? -> boolean) ((x =coord=) (y =coord=)
                                        (x1 =coord=) (y1 =coord=)
                                        (x2 =coord=) (y2 =coord=)
                                        &key ((allowed-error real) 0)
                                        (segment? nil))
  (let* ((linevec (line->vector x1 y1 x2 y2))
         (intvec (vector-right linevec))
         (p+vec (vector+ (cons x y) intvec))
         (nearest (line-intersection x1 y1 x2 y2 x y (car p+vec) (cdr p+vec)
                                     :a-segment? segment?)))
    (and nearest
         (< (dist^2 x y (car nearest) (cdr nearest)) allowed-error))))


(defun* line-intersection ((ax1 =coord=) (ay1 =coord=)
                           (ax2 =coord=) (ay2 =coord=)
                           (bx1 =coord=) (by1 =coord=)
                           (bx2 =coord=) (by2 =coord=)
                          &key ((a-segment? boolean) nil)
                           ((b-segment? boolean) nil)
                           ((integer? boolean) nil))
  "Returns the coordinates where the two lines A and B intersect, or nil if they
never meet. The lines are not line segments - they are assumed to extend
to infinity, unless A-SEGMENT? or B-SEGMENT? is TRUE."
  (:returns (or null =coord-pair=))
  (let ((a-gradient (line-gradient ax1 ay1 ax2 ay2))
        (a-intercept (line-y-intercept ax1 ay1 ax2 ay2))
        (b-gradient (line-gradient bx1 by1 bx2 by2))
        (b-intercept (line-y-intercept bx1 by1 bx2 by2))
        x y vertex)
    (setf vertex
          (cond
            ((and (null a-gradient) (null b-gradient))
             (return-from line-intersection nil))
            ((null a-gradient)
             (cons ax1 (+ (* b-gradient ax1) b-intercept)))
            ((null b-gradient)
             (cons bx1 (+ (* a-gradient bx1) a-intercept)))
            ((= a-gradient b-gradient)
             (return-from line-intersection nil))
            (t
             (setf x (/ (- b-intercept a-intercept)
                        (- a-gradient b-gradient) 1.0))
             (setf y (+ (* a-gradient x) a-intercept))
             (cons x y))))
    (cond
      ((and a-segment? (not (coordinates-in-area? (car vertex) (cdr vertex)
                                                    ax1 ay1 ax2 ay2)))
       nil)
      ((and b-segment? (not (coordinates-in-area? (car vertex) (cdr vertex)
                                                    bx1 by1 bx2 by2)))
       nil)
      (integer?
       (cons (floor (car vertex)) (floor (cdr vertex))))
      (t
       vertex))))


(defun* (points->lines -> list) ((points list))
  "Given an ordered list of coordinate pairs (X . Y), return a list
of line segments (X0 Y0 X1 Y1) connecting each point to the
next. The last point will connect to the first."
  (let ((lines nil))
    (iterate
     (with (lastx . lasty) = (last-elt points))
     (for i from 0 below (length points))
     (for (x . y) = (nth i points))
     (cond
       ((= i 0)
        (push (list lastx lasty x y) lines))
       (t
        (push (list (car (nth (1- i) points))
                    (cdr (nth (1- i) points))
                    x y) lines))))
    (reverse lines)))


;;;; <<Bresenham line algorithm>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(define-memo-function bresenham-line (x1 y1 x2 y2
                                         &key stop-test step-function)
  "Very fast method for drawing a straight line between two points on
a discrete grid. X1,Y1 is the starting square and X2,Y2 is the destination.
STOP-TEST, if supplied, is a function that accepts two arguments,
X and Y, and returns true if there is an 'obstacle' at X,Y and the
function should abort.
STEP-FUNCTION, if supplied, accepts arguments (X Y) and is called once
for each step on the line.
Returns a list of (X . Y) cons cells, starting at X1, Y1."
  (let ((dx (abs (- x2 x1)))
        (dy (abs (- y2 y1)))
        (x x1)
        (y y1)
        (offsetx (if (> x1 x2) -1 1))
        (offsety (if (> y1 y2) -1 1))
        err
        (trail (list)))
    (push (cons x y) trail)
    (if step-function (funcall step-function x y))
    (cond
      ((> dx dy)
       (setf err (floor dx 2))
       (iterate
         (while (and (/= x x2)
                     (or (null stop-test)
                         (not (funcall stop-test x y)))))
         (decf err dy)
         (when (< err 0)
           (incf y offsety)
           (incf err dx))
         (incf x offsetx)
         (push (cons x y) trail)
         (if step-function (funcall step-function x y))))
      (t
       (setf err (floor dy 2))
       (iterate
         (while (and (/= y y2)
                     (or (null stop-test)
                         (not (funcall stop-test x y)))))
         (decf err dx)
         (when (< err 0)
           (incf x offsetx)
           (incf err dy))
         (incf y offsety)
         (push (cons x y) trail)
         (if step-function (funcall step-function x y)))))
    (reverse trail)))


(defun* bresenham-line-vector ((x1 =coord=) (y1 =coord=)
                               (x2 =coord=) (y2 =coord=)
                               &key
                                 ((stop-test function) nil)
                                 ((step-function function) nil)
                                 ((path-max fixnum) 100)
                                 ((path-increment fixnum) 20)
                                 ((fail-on-obstacle? boolean) nil)
                                 ((skip-start? boolean) nil))
  (:returns (or null (vector (cons =coord= =coord=))))
  "Returns a vector of (X . Y) cons cells, starting at X1,Y1.
If FAIL-ON-OBSTACLE is true, return NIL if any obstacle is hit."
  (let ((dx (abs (- x2 x1)))
        (dy (abs (- y2 y1)))
        (x x1)
        (y y1)
        (offsetx (if (> x1 x2) -1 1))
        (offsety (if (> y1 y2) -1 1))
        err
        (trail (make-array path-max :fill-pointer 0 :adjustable t)))
    (flet* ((add-to-trail ((x =coord=) (y =coord=))
                          (vector-push-extend (cons x y) trail path-increment))
            (hit-obstacle? ((x =coord=) (y =coord=))
                           (:returns boolean)
                           (if (funcall stop-test x y)
                               (if fail-on-obstacle?
                                   (return-from bresenham-line-vector nil)
                                   t)
                               nil)))
      (unless skip-start?
        (add-to-trail x y))
      (if step-function (funcall step-function x y))
      (cond
        ((> dx dy)
         (setf err (floor dx 2))
         (iterate
           (while (and (/= x x2)
                       (or (null stop-test)
                           (not (hit-obstacle? x y)))))
           (decf err dy)
           (when (< err 0)
             (incf y offsety)
             (incf err dx))
           (incf x offsetx)
           (add-to-trail x y)
           (if step-function (funcall step-function x y))))
        (t
         (setf err (floor dy 2))
         (iterate
           (while (and (/= y y2)
                       (or (null stop-test)
                           (not (hit-obstacle? x y)))))
           (decf err dx)
           (when (< err 0)
             (incf x offsetx)
             (incf err dy))
           (incf y offsety)
           (add-to-trail x y)
           (if step-function (funcall step-function x y)))))
      trail)))


(defmacro do-bresenham-line ((xvar yvar x1 y1 x2 y2) &body body)
  `(bresenham-line ,x1 ,y1 ,x2 ,y2
                   :step-function
                   #'(lambda (,xvar ,yvar)
                       ,@body)))



(defmacro-clause (FOR coordvars :IN-LINE-FROM startcoords :TO endcoords)
  "Usage: FOR (x . y) IN-LINE-FROM (startx starty) TO (endx endy)) ..."
  (destructuring-bind (xvar . yvar) coordvars
    (destructuring-bind (startx starty endx endy) (append startcoords endcoords)
      `(for (,xvar . ,yvar) in (bresenham-line ,startx ,starty ,endx ,endy)))))



(defun* (bresenham-line-generator -> function)
    ((x1 fixnum) (y1 fixnum) (deltax fixnum) (deltay fixnum)
     &key ((stop-test (or null function)) nil))
  "* Arguments
- STARTX, STARTY :: Starting coordinates.
- DELTAX, DELTAY :: A vector describing the direction of the line.
- STOP-TEST :: If supplied, a function of the form (FN X Y) which
  returns non-nil if the coordinates X,Y are at or beyond the end
  of the line.
* Returns
A function taking no arguments.
* Description
Returns a closure which, when called, will return a pair of coordinates.
Each time the closure is called, the coordinate pair advances along
the line =(x1, y1) -> (x1 + deltax, y1 + deltay)=.
"
  (let* ((dx (abs deltax))
         (dy (abs deltay))
         (x x1)
         (y y1)
         (offsetx (sign deltax))
         (offsety (sign deltay))
         (err (if (> dx dy) (floor dx 2) (floor dy 2))))
    (lambda ()
      (cond
        ((> dx dy)
         (decf err dy)
         (when (< err 0)
           (incf y offsety)
           (incf err dx))
         (incf x offsetx)
         (if (or (null stop-test)
                 (not (funcall stop-test x y)))
             (cons x y)
             nil))
        (t
         (decf err dx)
         (when (< err 0)
           (incf x offsetx)
           (incf err dy))
         (incf y offsety)
         (if (or (null stop-test)
                 (not (funcall stop-test x y)))
             (cons x y)
             nil))))))


;;;; <<Random paths>> ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defparameter +MAX-RECTITUDE+ 6
  "Parameter used by MAKE-RANDOM-PATH. Must be a positive integer. A
higher rectitude produces a straighter path.")



(defun random-path-aux (x1 y1 x2 y2
                        &key
                          (rectitude 3)
                          (width 1)
                          (width-function nil)
                          (diagonal-ok? t))
  (cond
    ((and (<= (abs (- x1 x2)) 1)
          (<= (abs (- y1 y2)) 1))
     nil)
    (t
     (let* ((displace
              (vector* (funcall (if (= 1 (random 2)) #'vector-right #'vector-left)
                                (cons (- x2 x1) (- y2 y1)))
                       (/ 1 (rand-between rectitude +MAX-RECTITUDE+))))
            (vec (vector+ (cons (/ (- x2 x1) 2) (/ (- y2 y1) 2))
                          displace))
            (x3 (if (or diagonal-ok?
                        (>= (floor (car vec)) (floor (cdr vec))))
                    (+ x1 (floor (car vec)))
                    x1))
            (y3 (if (or diagonal-ok?
                        (> (floor (cdr vec)) (floor (car vec))))
                    (+ y1 (floor (cdr vec)))
                    y1))
            (newcoords (list (cons x3 y3))))
       (let ((width1 (or (if width-function (funcall width-function x1 y1))
                         width))
             (width/2 0))
         (when (and width1
                    (> width1 1))
           (setf width/2 (floor width1 2))
           (loop for x from (- x1 width/2) upto (+ x1 width/2)
                 do (loop for y from (- y1 width/2) upto (+ y1 width/2)
                          do (pushnew (cons x y) newcoords)))))
       (let ((width2 (or (if width-function (funcall width-function x2 y2))
                         width))
             (width/2 0))
         (when (and width2
                    (> width2 1))
           (setf width/2 (floor width2 2))
           (loop for x from (- x2 width/2) upto (+ x2 width/2)
                 do (loop for y from (- y2 width/2) upto (+ y2 width/2)
                          do (pushnew (cons x y) newcoords)))))
       (setf rectitude (min +MAX-RECTITUDE+ (1+ rectitude)))
       (append newcoords
               (random-path-aux x1 y1 x3 y3
                                :rectitude rectitude :width width
                                :width-function width-function)
               (random-path-aux x3 y3 x2 y2
                                :rectitude rectitude :width width
                                :width-function width-function))))))


(defun random-path (x1 y1 x2 y2
                    &key (rectitude 3)
                         (width 1)
                         (width-function nil)
                         (diagonal-ok? t))
  "Return a list of (X . Y) integer pairs, prepresenting a randomly meandering
path from X1,Y1 to X2,Y2.
RECTITUDE is a positive integer
If WIDTH-FUNCTION is supplied, it must be a function that accepts two
arguments, X and Y, and returns a positive integer representing the desired
width at point (X . Y)."
  (remove-duplicates
   (append (list (cons x1 y1) (cons x2 y2))
           (random-path-aux x1 y1 x2 y2 :rectitude rectitude :width width
                                        :width-function width-function
                                        :diagonal-ok? diagonal-ok?))
   :test 'equal))


;; (defun random-path2 (x1 y1 x2 y2 &key (diagonal-ok? t))
;;   (iterate
;;     (with points = (iterate
;;                      (for i from 1 to 3)
;;                      (collect (cons (rand-between (1+ x1) (1- x2))
;;                                     (rand-between (1+ y1) (1- y2))))))
;;     (for (x . y)
;;          :initially (cons x1 y1)
;;          :then (coords-closer x y x2 y2 :diagonal-ok? diagonal-ok?))
;;     (collect (cons x y))
;;     (format t "~S ~S~%" x y)
;;     (until (and (= x x2) (= y y2)))))


(defun* (bezier-curve-quadratic -> (cons real real))
    ((tee (real 0 1)) (x0 real) (y0 real)
     (x1 real) (y1 real) (x2 real) (y2 real))
  "Function to calculate the coordinates of a quadratic (second-order)
Bezier curve, given initial point P0, final point P2, and another anchor
point P1. The parameter TEE ranges between 0 and 1. When TEE = 0, the
function returns (X0,Y0). When TEE = 1, the function returns (X2,Y2)."
  (cons (+ (* x0 (* (- 1 tee) (- 1 tee)))
           (* x1 2 (- 1 tee))
           (* x2 tee tee))
        (+ (* y0 (* (- 1 tee) (- 1 tee)))
           (* y1 2 (- 1 tee))
           (* y2 tee tee))))

;; given x, find y on the Bezier curve:
;; (bezier-curve-quadratic (abs (/ x (- xmin xmax))) x0 y0 ...)


;;;; <<Polygons>>  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun* (area-of-polygon -> real) ((vertices list))
  "* Arguments
- VERTICES :: a list of =(X . Y)= coordinate pairs, ordered to move around the
edge of the polygon in a clockwise direction.

* Returns
The area of the polygon outlined by =VERTICES=.

* Description
Simple algorithm to calculate the area of an irregularly shaped polygon using a
directed list of its vertices. The vertices need to follow one another in a
clockwise direction, otherwise the area returned will be negative.  From:
[[http://alienryderflex.com/polygon_area/]]."
  (* 0.5
     (iterate
       (with num-points = (length vertices))
       (for i from 0 below num-points)
       (for j from 1 to num-points)
       (for (x0 . y0) = (nth i vertices))
       (for (x1 . y1) = (nth (mod j num-points) vertices))
       (summing (* (+ x0 x1) (- y0 y1))))))


(defun* (point-in-polygon? -> boolean) ((px =coord=) (py =coord=)
                                        (points list)
                                        (include-borders-in-polygon? boolean))
  ;; A point P lies inside a closed polygon if the number of intersections
  ;; between a horizontal ray passing through P, and the line segments
  ;; making up the polygon, is ODD.
  (let ((count 0)
        (lines (points->lines points)))
    (iterate
      (for (x1 y1 x2 y2) in lines)
      (for intersect = (line-intersection px py (1+ px) py x1 y1 x2 y2
                                          :b-segment? t))
      (cond
        ((null intersect)
         (next-iteration))
        ((and (= (car intersect) px)
              (= (cdr intersect) py))
         (if include-borders-in-polygon?
             (incf count)))
        ((> (car intersect) px)
         (incf count))))
    (oddp count)))


(defun* (polygons-overlap? -> boolean) ((poly1 list) (poly2 list)
                                        &optional
                                          (include-borders-in-polygon? t))
  (or (find-if (lambda (coords)
                 (point-in-polygon? (car coords) (cdr coords) poly2
                                    include-borders-in-polygon?))
               poly1)
      (find-if (lambda (coords)
                 (point-in-polygon? (car coords) (cdr coords) poly1
                                    include-borders-in-polygon?))
               poly2)))


(defun* (polygon-bounds -> (values =coord= =coord= =coord= =coord=))
    ((vertices list))
  "* Arguments:
- VERTICES: Clockwise ordered list of (X . Y) coordinates.
* Returns: Four values: MINX MINY MAXX MAXY"
  (values (car (least #'car vertices))
          (cdr (least #'cdr vertices))
          (car (greatest #'car vertices))
          (cdr (greatest #'cdr vertices))))


(defun random-coordinates-in-polygon (num vertices)
  (multiple-value-bind (minx miny maxx maxy) (polygon-bounds vertices)
    (iterate
      (with cnt = 0)
      (with x = nil)
      (with y = nil)
      (while (< cnt num))
      (setf x (rand-between minx maxx))
      (setf y (rand-between miny maxy))
      (when (point-in-polygon? x y vertices t)
        (collect (cons x y))
        (incf cnt)))))


(defun* print-polygons ((polygons list)
                        &key (coord-test nil) (coord-printer nil))
  "COORD-TEST : if this is given, it must be a predicate that takes X and Y
arguments. It should return true if the point X,Y should be printed by
COORD-PRINTER rather than by PRINT-BOXES."
  (flet ((default-coord-printer (x y)
           (declare (ignorable x y))
           " ")
         (least-x (coords) (car (least #'car coords)))
         (least-y (coords) (cdr (least #'cdr coords)))
         (greatest-x (coords) (car (greatest #'car coords)))
         (greatest-y (coords) (cdr (greatest #'cdr coords))))
    (let ((minx (apply #'min (mapcar #'least-x polygons)))
          (miny (apply #'min (mapcar #'least-y polygons)))
          (maxx (apply #'max (mapcar #'greatest-x polygons)))
          (maxy (apply #'max (mapcar #'greatest-y polygons))))
      (flet ((print-coord (x y)
               (let* ((polys-here
                        (remove-if-not
                         (lambda (poly) (point-in-polygon? x y poly t))
                         polygons))
                      (match (least #'area-of-polygon polys-here)))
                 (cond
                   ((and coord-test
                         (funcall coord-test x y))
                    (format t "~A" (funcall (or coord-printer
                                                #'default-coord-printer)
                                            x y)))
                   (t
                    (format t "~C"
                            (code-char
                             (clamp (+ 32 (if match
                                              (1+ (position match polygons)) 0))
                                    32 126))))))))
        (cond
          (*invert-y-axis?*
           (iterate
             (for y from miny to maxy)
             (format t "~v,' D: " (ceiling (log (abs maxy) 10)) y)
             (iterate
               (for x from minx to maxx)
               (print-coord x y)
               (finally (fresh-line)))))
          (t
           (iterate
             (for y from maxy to miny)
             (format t "~v,' D: " (ceiling (log (abs maxy) 10)) y)
             (iterate
               (for x from minx to maxx)
               (print-coord x y)
               (finally (fresh-line))))))
        (iterate
          (for poly in polygons)
          (format t "\"~C\" = ~A~%"
                  (code-char (+ 32 (if poly (1+ (position poly polygons)) 0)))
                  poly)))
      polygons)))



;;;; cartesian.lisp ends here ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
